require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.empty? ? 0 : arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? do |long_string|
    sub_string?(long_string, substring)
  end
end

def sub_string?(word, substring)
  word.include?(substring)
end

def is_letter?(char)
  /[a-z]/.match(char) != nil
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  non_unique = string.chars.select do |letter|
    is_letter?(letter.downcase) && string.count(letter) > 1
  end
  non_unique.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def remove_punctuation(word)
  letters = word.chars
  letters.select do |letter|
    is_letter?(letter.downcase)
  end.join("")
end


def longest_two_words(string)
  words = string.split.map! do |word|
    remove_punctuation(word)
  end
  words.sort_by! {|word| word.length}
  words[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  alphabet = "a".upto("z").to_a
  letters = string.chars
  alphabet - letters
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  first_yr.upto(last_yr).select do |year|
    not_repeat_year?(year)
  end
end

def not_repeat_year?(year)
  digits = year.to_s.split("")
  digits_without_dups = digits.select do |digit|
    digits.count(digit) == 1
  end
  digits.length == digits_without_dups.length
end


# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  wonders = songs.select do |song|
    no_repeats?(song, songs) == false
  end.uniq
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |song, i|
    if song == song_name
      return true if song == songs[i + 1]
    end
  end

  false
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  words = string.split(" ").map { |word| remove_punctuation(word) }
  c_words = words.reject {|word| c_distance(word) == nil }
  c_words.reduce do |word_one, word_two|
    if c_distance(word_one) > c_distance(word_two)
      word_one = word_two
    else
      word_one
    end
  end
end

def c_distance(word)
  letters = word.split("")
  letters.reverse_each.with_index do |letter, neg_i|
    return neg_i + 1 if letter == "c"
  end
  nil
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  ranges = []
  range = []
  arr.each_with_index do |int, i|
    if repeat?(arr, i)
      range << i
    else
      if range.empty? == false
        range << i
        ranges << [range.first, range.last]
      end
      range = []
    end
  end
  ranges
end

def repeat?(arr, index)
  arr[index] == arr[index + 1]
end
